const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({ path: './config.env' })
const app = require('./app')

const DB = process.env.DATABASE.replace(
  'PASSWORD',
  process.env.DATABASE_PASSWORD,
)
// console.log(process.env.DATABASE_PASSWORD)
mongoose.connect(DB).then((con) => {
  console.log(con.connections)
  console.log('DB connection succesful')
}).catch(error => console.log(error));

/* Starting the server on port 4000. */
const port = 4000
app.listen(port, () => {
  console.log(`App running on port ${port} ..`)
})
