const path = require('path')
const AppError = require('./../utils/appError')
const News = require('../models/newsModel')
const { nextTick } = require('process')
const catchAsync = require('../utils/catchAsync')

exports.getLoginForm = (req, res) => {
  res.sendFile(path.join(__dirname, '../', 'public', 'login.html'))
}

exports.getSignupForm = (req, res) => {
  res.sendFile(path.join(__dirname, '../', 'public', 'signup.html'))
}

// This is for home and news
exports.getHome = (req, res) => {
  res.sendFile(path.join(__dirname, '../', 'public', 'base_dashboard.html'))
}
exports.getAccount = (req, res) => {
  res.sendFile(path.join(__dirname, '../', 'public', 'account.html'))
}

exports.getNews = catchAsync(async (req, res) => {
  // 1) Get the data, for the requested news (including reviews)
  const news = await News.findOne({ slug: req.params.slug }).populate({
    path: '',
  })
  // 2) Build template
  // 3) Render template using data from 1)
  res.status(200).render('news', {
    title: 'Covid-19 pandemic',
    news,
  })
})

// if (!News) {
//   return nextTick(new AppError('There is no newwith that name.', 404))
// }
