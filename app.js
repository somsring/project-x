/* Importing the express module. */
const path = require('path')
const express = require('express')
const app = express()
const mongoSanitize = require('express-mongo-sanitize')
const xss = require('xss-clean')
const cors = require('cors')
const cookieParser = require('cookie-parser')

const newsRouter = require('./routes/newsRoutes')
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
/* Middleware*/
app.use(express.json())
// Implement CORS
app.use(cors())
app.use(cookieParser())
app.use((req, res, next) => {
  req.requestTime = new Date().toISOString()
  //accesing headers
  console.log(req.cookies)
  next()
})
app.use('/', viewRouter)
app.use('/api/v1/news', newsRouter)
app.use('/api/v1/users', userRouter)

// Data Sanitization against NoSQL query injection
app.use(mongoSanitize())
// Data sanitization against XSS
app.use(xss())
// Serving static files
app.use(express.static(path.join(__dirname, 'public')))

module.exports = app
