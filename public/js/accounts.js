var obj = JSON.parse(document.cookie.substring(6))

var el = document.querySelector('.admin-nav')
// console.log(user)
// console.log(user[8])
// console.log(typeof user[8])
if (obj.role == 'admin') {
  el.innerHTML =
    '<h5 class="admin-nav__heading">Admin</h5><ul class="side-nav"><a href="#"><svg><use xlink:href="img/icons.svg#icon-users"></use> </svg>Manage users</a></li> <li><a href="#"><svg><use xlink:href="img/icons.svg#icon-star"></use></svg>Manage News</a></li><li><a href="#"><svg><use xlink:href="img/icons.svg#icon-briefcase"></use></svg>Manage E-Services</a></li></ul>'
} else if (obj.role == 'sme') {
  el.innerHTML =
    '<h5 class="admin-nav__heading">Subject Matter Expert</h5><ul class="side-nav"><li><a href="#"><svg><use xlink:href="img/icons.svg#icon-star"></use></svg>Manage News</a></li></ul>'
} else if (obj.role == 'pharmacist') {
  el.innerHTML =
    '<h5 class="admin-nav__heading">Pharmacist</h5><ul class="side-nav"><li><a href="#"><svg><use xlink:href="img/icons.svg#icon-star"></use></svg>Manage E-Services</a></li></ul>'
} else {
  el.innerHTML =
    '<h5 class="admin-nav__heading">User</h5><ul class="side-nav"><a href="#"><svg><use xlink:href="img/icons.svg#icon-users"></use> </svg>Manage Medical Reminder</a></li> <li><a href="#"><svg><use xlink:href="img/icons.svg#icon-briefcase"></use></svg>Manage E-Services</a></li></ul>'
}
// }

var el1 = document.querySelector('.form.form-user-data')
el1.innerHTML =
  ` <div class="form__group"> <label class="form__label" for="name">Name</label> <input class="form__input" id="name" type="text" value="` +
  obj.name.toUpperCase() +
  `" required="required" name="name"/></div><div class="form__group ma-bt-md"><label class="form__label" for="email">Email address</label>
<input class="form__input" id="email" type="email" value="` +
  obj.email +
  `" required="required" name="email"/>
</div><div class="form__group form__photo-upload"><img class="form__user-photo" src="../img/users/` +
  obj.photo +
  `" alt="User photo"/><input class="form__upload" type="file" accept="image/*" id="photo" name="photo"/><label for="photo">Choose new photo</label></div>
<div class "form__group right">                                                                                                    <button class="btn btn--small btn--green">Save settings</button></div>`

// el1.addEventListener('submit', (e) => {
//     e.preventDefault()
//     const email = document.getElementById('email').value
//     const password = document.getElementById('password').value
//     login(email, password)
//   })
