// updateData
// import 'axios' from 'axios';

import { showAlert } from './alert.js'

// type is either 'password' or data
export const updateSettings = async (data, type) => {
  try {
    const url =
      type === 'password'
        ? 'http://localhost:4000/api/v1/users/updateMyPassword'
        : 'http://localhost:4000/api/v1/users/updateMe'
    const res = await axios({
      method: 'PATCH',
      url,
      data,
    })
    console.log(res.data.status)
    if (res.data.status === 'success') {
      showAlert('success', 'Data updated successfully!')
    }
  } catch (err) {
    let message =
      typeof err.response !== 'undefined'
        ? err.response.data.message
        : err.message
    // showAlert('error', 'Error: Please provide valid email address', message)
    showAlert('error', err.response.data.message)
  }
}

const userDataForm = document.querySelector('.form.form-user-data')
userDataForm.addEventListener('submit', (e) => {
  e.preventDefault()
  var obj = JSON.parse(document.cookie.substring(6))
  const form = new FormData()
  form.append('name', document.getElementById('name').value)
  form.append('email', document.getElementById('email').value)
  form.append('photo', document.getElementById('photo').files[0])
  form.append('userId', obj._id)
  console.log(form)
  updateSettings(form, 'data')
})

const userPasswordForm = document.querySelector('form.form-user-password')
userPasswordForm.addEventListener('submit', async (e) => {
  e.preventDefault()

  document.querySelector('.btn--save-password').textContent = 'Updating...'
  const passwordCurrent = document.getElementById('password-current').value
  const password = document.getElementById('password').value
  const passwordConfirm = document.getElementById('password-confirm').value
  await updateSettings(
    { passwordCurrent, password, passwordConfirm },
    'password',
  )
  document.querySelector('.btn--save-password').textContent = 'Save password'
  document.getElementById('password-current').value = ''
  document.getElementById('password').value = ''
  document.getElementById('password-confirm').value = ''
})
