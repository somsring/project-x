import { showAlert } from './alert.js'

// Logging Out

const logout = async () => {
  try {
    const res = await axios({
      method: 'GET',
      url: 'http://localhost:4000/api/v1/users/logout',
    })
    if (res.data.status === 'success') {
      console.log('Hi logout')
      location.reload(true)
    }
  } catch (err) {
    showAlert('error', 'Error logging out! Try again.')
  }
}
var obj
if (document.cookie) {
  obj = JSON.parse(document.cookie.substring(6))
} else {
  obj = JSON.parse('{}')
}

// var user = document.cookie.split(/(\s+)/)

console.log(obj._id)
var el = document.querySelector('.nav.nav--user')
// showHideUser()
// function showHideUser() {
// console.log(Object.keys(user))

if (obj._id) {
  el.innerHTML =
    '<a id = "logout" class="nav__el">Log out</a> <a href="/me" class="nav__el"><img src="../img/users/' +
    obj.photo +
    '" alt="Phot0 of ${user.name}" class="nav__user-img" /><span>' +
    obj.name +
    '</span> </a>'
  var doc = document.querySelector('#logout')

  doc.addEventListener('click', (e) => logout())
} else {
  el.innerHTML =
    '<a class="nav__el nav__el--cta" href="/login">Log in</a> <a class="nav__el nav__el--cta" href="/signup">Sign up</a>'
}
// }

const allNews = async () => {
  //alert(email, password)
  //axios for http request
  // console.log(email, password)
  try {
    const res = await axios({
      method: 'GET',
      url: 'http://localhost:4000/api/v1/news/allNews',
      data: {
        // email,
        // password,
      },
    })
    // console.log(res.data)
    displayNews(res.data)
    // news = res.data
  } catch (err) {
    console.log(err)
  }
}
allNews()
console.log(document.cookie)

const displayNews = (news) => {
  var arr = news.data.news1
  for (let i = 0; i < arr.length; i++) {
    var card = document.querySelector('.card').cloneNode(true)
    var el1 = card.querySelector('.card__picture-img')
    var el2 = card.querySelector('.card__sub-heading')
    var el3 = card.querySelector('.card__text')
    var el4 = card.querySelector('.publishedAt')
    var el5 = card.querySelector('.publishedBy')
    const element = arr[i]
    el1.innerHTML = '' + element.imageCover
    el2.innerHTML = '' + element.title
    el3.innerHTML = '' + element.description
    var d = new Date(element.publishedAt)
    el4.innerHTML =
      '' +
      d.toLocaleString('en-US', {
        month: 'long',
        year: 'numeric',
      })
    el5.innerHTML = '' + element.author
    var card2 = document.querySelector('.card')
    card2.insertAdjacentElement('afterend', card)
    console.log(element)
  }
  document.querySelector('.card').remove()
}
