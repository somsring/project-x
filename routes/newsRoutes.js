const express = require('express')
const newsFeedsController = require('./../controllers/newsFeedsController')
const authController = require('./../controllers/authController')
const router = express.Router()

router.get('/allNews', authController.protect, newsFeedsController.getAllNews)

router
  .route('/')
  .get(authController.protect, newsFeedsController.getAllNews)
  .post(newsFeedsController.createNews)

router
  .route('/:id')
  .get(newsFeedsController.getNews)
  .patch(newsFeedsController.updateNews)
  .delete(
    authController.protect,
    authController.restrictTo('sme'),
    newsFeedsController.deleteNews,
  )

module.exports = router
