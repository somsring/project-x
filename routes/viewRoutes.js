const express = require('express')

const router = express.Router()
const viewsController = require('./../controllers/viewsController')
const authController = require('./../controllers/authController')

// router.use(authController.isLoggedIn)

router.get('/', authController.isLoggedIn, viewsController.getHome)
router.get('/login', authController.isLoggedIn, viewsController.getLoginForm)
router.get('/signup', authController.isLoggedIn, viewsController.getSignupForm)
router.get('/me', authController.protect, viewsController.getAccount)
router.get('/news/:slug', viewsController.getNews)
module.exports = router
