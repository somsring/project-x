const mongoose = require('mongoose')

const medicalReminderSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: [true, 'Reminder must belong to a User!'],
  },
  Subject: {
    type: String,
    required: [true, 'A reminder must have a subject'],
    validate: [validator.isAlpha, 'A subject must only contain characters'],
  },
  remindAt: {
    type: Date,
    require: [true, 'Reminder must have a specific date.'],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  isDeleted: {
    type: Boolean,
    default: true,
  },
})

const reminder = mongoose.model('MedicalReminder', medicalReminderSchema)

module.exports = reminder
