// eService // createdAt // ref to the users

const mongoose = require('mongoose')

const eServiceSchema = new mongoose.Schema({
  service: {
    type: String,
    required: [true, 'service can not be empty!'],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  pharmacistReply: {
    type: String,
  },
  forPharmacist: {
    type: String,
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: [true, 'Service must belong to a user'],
  },
})

const service = mongoose.model('Service', eServiceSchema)

module.exports = service
