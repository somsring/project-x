const mongoose = require('mongoose')
const validator = require('validator')

const newsFeedsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'A name should be unique'],
  },
  description: {
    type: String,
    required: true,
    trim: true,
    maxLength: [
      30,
      'A news description must have less or equal than 40 characters',
    ],
    minLength: [
      10,
      'A news description must have more or equal than 10 characters',
    ],
  },
  publishedAt: {
    type: Date,
    default: Date.now(),
  },
  content: {
    type: String,
    required: true,
    trim: true,
  },
  imageCover: {
    type: String,
    required: [true, 'A tour must have a cover image'],
  },
  images: [String],
  isPublished: {
    type: Boolean,
    default: false,
  },
  publishedBy: {
    type: String,
  },
  //SME: Subject Matter Experts
  SME: [
    {
      type: mongoose.Schema.ObjectId,
      ref: 'User',
    },
  ],
})
const newsFeed = mongoose.model('NewsFeeds', newsFeedsSchema)
module.exports = newsFeed
